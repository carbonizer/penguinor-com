from multilogr import BaseMultilogr

import penguinor.config as cfg

__version__ = '0.2rc2'


class Multilogr(BaseMultilogr):
    LOGS_SPEC = cfg.LOGS_SPEC
