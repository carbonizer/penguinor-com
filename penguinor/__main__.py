"""
penguinor.__main__
==================

The main entry-point for the package
"""
import argparse
import sys

from penguinor import __version__, cfg, Multilogr
from penguinor.site.__main__ import config_web_parser, web_cmd

logr = Multilogr.get_logr('pnr')

# Reference directly so it isn't flagged as an unused import
if web_cmd:
    pass


def parse_args(args):
    """Parse program arguments

    The program comprises multiple commands.  Each command has its own
    entry-point and sub-parser.

    The entry-point function is named ``<command>_cmd``, and it takes the
    parsed argument namespace ``pargs`` as its sole argument.  If it has a
    return value, it is a string that will be printed to ``stdout``.

    The sub-parser config function is named ``config_<command>_parser``, and it
    takes the :class:`argparse.ArgumentParser` ``parser`` that it configures as
    its only argument.  At minimum, the function must set the entry-point
    function with an expression of the form::

        parser.set_defaults(func=<command_func>, ...)

    Args:
        args (tuple[str]): Arguments to parse

    Returns:
        argparse.Namespace: Parsed namespace

    """
    parser = argparse.ArgumentParser()

    # Any global arguments would be added here
    parser.add_argument('--version', action='version',
                        version='{} {}'.format(cfg.PKG_NAME, __version__))

    # Configure subparsers
    subparsers = parser.add_subparsers(title='commands')
    for config_func in (
            config_web_parser,
    ):
        name = config_func.__name__.split('_', 2)[1]

        # Help string is the first line of the doc string of the cmd func
        cmd_func = globals().get(name + '_cmd')
        help_str = cmd_func.__doc__.splitlines()[0]

        config_func(subparsers.add_parser(name, help=help_str))

    # Placeholder func argument
    parser.set_defaults(func=None)

    pargs = parser.parse_args(args)

    # If no command has been specified, print help
    if not pargs.func:
        parser.parse_args(['-h'])

    return pargs


def main(args=sys.argv[1:]):
    pargs = parse_args(args)
    logr.info('Starting {}'.format(cfg.PKG_NAME))
    result = pargs.func(pargs)
    print(result)
    logr.info('{} finished successfully'.format(cfg.PKG_NAME))


if __name__ == '__main__':
    main()
