"""
penguinor.site.routes
=====================

Configures the routes of the application

Routes are usually configured on the global app instance directly.  However,
doing so in functions aids unit testing a debugging.
"""
import flask

from penguinor.site.aboutview import AboutView
from penguinor.site.welcomeview import WelcomeView


def config_hello_route(app):
    """Configure the single route, hello

    Args:
        app (flask.Flask): App to configure

    Returns:
        flask.Flask: Same object that was passed in
    """

    @app.route('/hello')
    def hello():
        """Good to keep around for sanity checks"""
        return 'Hello World!'

    return app


def config_routes(app):
    """Configure all app routes

    Args:
        app (flask.Flask): App to configure

    Returns:
        flask.Flask: Same object that was passed in
    """
    config_hello_route(app)

    app.add_url_rule('/about', view_func=AboutView.as_view('about'))
    app.add_url_rule('/welcome', view_func=WelcomeView.as_view('welcome'))

    @app.route('/')
    def my_index():
        return flask.redirect(flask.url_for('welcome'))

    return app
