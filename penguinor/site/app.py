"""
penguinor.site.app
==================

Create and configure the global app instance

Production servers usually need to access the global app instance.
"""
from werkzeug.wsgi import DispatcherMiddleware

from thekraf.flaskapp.app import app as thekraf_app

from penguinor.site import create_app
from penguinor.site.routes import config_hello_route, config_routes


def create_and_config_hello_app():
    """Create and configure a simple app, helpful for some unit testing

    Returns:
        flask.Flask: Simple hello app
    """
    return config_hello_route(create_app())


def create_and_config_app():
    """Create and configure the main app

        Returns:
            flask.Flask: Main app instance
    """
    app_ = config_routes(create_app())

    return DispatcherMiddleware(app_, {
        '/thekraf': thekraf_app,
    })


app = create_and_config_app()
"""flask.Flask: Global flask app instance"""
