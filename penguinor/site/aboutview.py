"""
penguinor.site.aboutview
==========================

About the author of the site
"""
from penguinor.site.baseview import ViewBase
from penguinor import Multilogr

logr = Multilogr.get_logr('web')


class AboutView(ViewBase):
    pass
