var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: './app.coffee',
    output: {
        path: path.join(__dirname, '..', 'static'),
        publicPath: '../static/',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {test: /\.coffee$/, loader: 'coffee-loader'},
            {test: /\.css$/, loader: 'style!css'},
            {test: /\.sass$/, loader: 'style!css!sass?indentedSyntax=true'},
            // the url-loader uses DataUrls.
            // the file-loader emits files.
            {
                test: /\.(woff|woff2)$/,
                loader: "url-loader?name=[path][name].[ext]?[hash]&limit=10000&mimetype=application/font-woff"
            },
            {
                test: /\.(eot|svg|ttf)$/,
                loader: "file-loader?name=[path][name].[ext]?[hash]"
            }
        ]
    },
    resolve: {
        extensions: ['', '.web.coffee', '.web.js', '.coffee', '.js']
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        })
    ]
};
