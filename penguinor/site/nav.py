"""
penguinor.site.nav
==================

Configuration of the navigation bar elements
"""
import flask


def _nav_elems_from_endpoints(*endpoints):
    """Create nav element dicts for the templates

    Args:
        *endpoints (str): Endpoint names

    Returns:
        list[dict]: Nav element dicts
    """
    mapped_endpoints = tuple(
        flask.current_app.url_map._rules_by_endpoint.keys()
    )
    elems = []
    for endpoint in endpoints:
        if '.' in endpoint:
            name = endpoint.rsplit('.', 1)[1]
        elif ':' in endpoint:
            name = endpoint.rsplit(':', 1)[1]
        else:
            name = endpoint
        elem = {
            'name': name,
            'endpoint': endpoint,
            'title': name.replace('_', ' ').title(),
            'href': flask.url_for(endpoint)
            if endpoint in mapped_endpoints else '#',
            'disabled': False,
            'active': flask.request.endpoint == endpoint,
        }
        elems.append(elem)

    return elems


def top_nav_elems():
    """Configure the top navbar elements for the template

    Returns:
        tuple[dict[str, str]]: Info for each element
    """
    elems = _nav_elems_from_endpoints(
        'about',
        'thekraf',
    )

    for elem in elems:
        if elem['endpoint'] == 'about':
            elem['title'] = 'Andrew'
        if elem['endpoint'] == 'thekraf':
            elem['title'] = 'thekraf'
            elem['href'] = '/thekraf'

    return elems
