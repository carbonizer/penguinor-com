"""
penguinor.site.welcomeview
==========================

Landing page for the site
"""
from penguinor.site.baseview import ViewBase
from penguinor import Multilogr

logr = Multilogr.get_logr('web')


class WelcomeView(ViewBase):
    pass
