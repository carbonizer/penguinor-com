# Andrew M. Scarr
  
**carbonizer@mac.com**

### Education

#### Personal Enrichment Courses <small>*(Mar 2013 to Present, in order taken)*</small>
    
*Cryptography | Internet History, Technology, and Security |
Linear Algebra through Computer Science Applications |
Introduction to Systematic Program Design - Part 1 |
Introduction to Power Electronics*
    
Stanford University; University of Michigan; Brown University;
The University of British Columbia; University of Colorado Boulder -
via coursera.org

#### Graduate Work in Physics / Coursework for Physics and Mathematics Teaching License <small>*(Aug 2006 to Jun 2008)*</small>

University of Cincinnati - Cincinnati, OH

#### Bachelor of Science in Electrical Engineering (Minors in Mathematics and Physics) <small>*(Sep 2001 to Jun 2006)*</small>

University of Cincinnati - Cincinnati, OH

### Work Experience 

#### Senior Software / Research & Electronics Engineer <small>*(Aug 2011 to Sep 2015)*</small>

*Constellation Data Systems - Cincinnati, OH*

Architecting and developing full stack for remote data access applications, from
firmware in C on embedded ARM systems, to the backend Python code and database
running on single-board Linux systems, to the frontend JavaScript/HTML5/CSS3
code served to the client.  Administrating said single-board Linux systems.
Managing engineering tests and hardware builds.  Developing Python code for
engineering data analysis.

#### Mathematics Teacher then Substitute Teacher <small>*(Aug 2008 to Aug 2011)*</small>

*The Seven Hills Schools then various school districts - Cincinnati, OH*

Teaching Precalculus and Algebra II. Tutoring all levels of mathematics during
free bells and lunches.

Substituting for teachers of all subjects from 6th to 12th grade.

### Special Knowledge

*Each category is roughly ordered by amount of experience*

**OS/Platforms:** Mac OS X, Linux (esp. Debian-based), iOS, BeagleBone,
Raspberry Pi, U-Boot, Windows, Android

**Python:** Python 2/3, PEP8, IPython[ Notebook]/Jupyter, pyenv, virtualenv,
nose, doctest, Flask, Jinja2, Django, SQLAlchemy, Sphinx, Celery, distutils,
matplotlib, WxWidgets

**C:**	C, C++, ARM, clang, GCC, make, CMake, Check, C#

**CLI:** Zsh, Bash, coreutils, shell scripting, startup scripts, ssh, vim, tmux,
Docker, Vagrant

**Web:** ECMAScript 6/2015, JavaScript, CoffeeScript, JQuery, YAML, JSON,
Bootstrap, HAML, Grunt, Node.js, Sass, CSS, webpack, React, Handlebars, Ember,
Angular 2, PhantomJS

**Markup:** Markdown, Pandoc, reStructedText, LaTeX

**Other Prog.:** Mercurial, Git, SQLite, Device Tree, Racket, AppleScript,
Objective-C, Mathematica

**Misc.:** Layered bitmap and vector graphics editing
