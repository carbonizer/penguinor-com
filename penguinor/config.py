"""
penguinor.config
================

Global configuration for the whole package
"""
import inspect
import misaka
import os

PKG_NAME = 'penguinor'

PKG_ROOT = os.path.dirname(
    os.path.abspath(inspect.getfile(inspect.currentframe())))

PKG_VAR_ROOT = os.path.expandvars(os.path.join(
    '$HOME',
    '.' + PKG_NAME,
))

PKG_LOG_ROOT = os.path.join(PKG_VAR_ROOT, 'logs')

# site config
PKG_WEB_ROOT = os.path.join(PKG_ROOT, 'site')

WEB_HOST = '0.0.0.0'
WEB_PORT = 5000

SECRET_KEY_PATH = os.path.join(PKG_VAR_ROOT, 'secret_key')

SERVER_CRT_PATH = os.path.join(PKG_VAR_ROOT, 'server.crt')
SERVER_KEY_PATH = os.path.join(PKG_VAR_ROOT, 'server.key')


class FlaskAppConfig(object):
    # http://flask.pocoo.org/docs/latest/config/
    LOGGER_NAME = 'web'


# multilogr config
LOGS_SPEC = {
    'pnr': {
        'log_path': os.path.join(PKG_LOG_ROOT, PKG_NAME + '.log'),
        'levels': {
            'console': 'DEBUG',
            'file': 'DEBUG',
        },
    },
    FlaskAppConfig.LOGGER_NAME: {
        'log_path': os.path.join(PKG_LOG_ROOT,
                                 FlaskAppConfig.LOGGER_NAME + '.log'),
        'levels': {
            'console': 'DEBUG',
            'file': 'DEBUG',
        },
    },
    'werkzeug': {
        'log_path': os.path.join(PKG_LOG_ROOT,
                                 FlaskAppConfig.LOGGER_NAME + '.log'),
        'levels': {
            'console': 'DEBUG',
            'file': 'DEBUG',
        },
    },
}


def _load_markdown():
    renderer = misaka.Markdown(misaka.HtmlRenderer())
    name_parts_pairs = (
        ('resume', (PKG_WEB_ROOT, 'static', 'scarr-andrew-cv.md')),
    )
    d = {}
    for name, parts in name_parts_pairs:
        path = os.path.join(*parts)
        with open(path) as fp:
                md = fp.read()
        d[name] = renderer(md)
    return d

HTML = _load_markdown()


for dirpath in (PKG_VAR_ROOT, PKG_LOG_ROOT):
    if not os.path.exists(dirpath):
        os.mkdir(dirpath, 0o755)
