# An image can be built in ~3 minutes (assuming the python image is already
# downloaded) from this file if from this directory you run a command like
#   docker build -t penguinor .
# Then, you can run the image with
#   docker run --rm -it -p 5000:5000 penguinor
# Note, if you are not running on linux, you'll need the address of your docker
# machine to connect to server which you can get with a command of the form
#   docker-machine ip <machine-name>

FROM python:3.5.1

ENV PKG_NAME='penguinor'

ARG tag='v0.2rc2'
ARG thekraf_tag='v0.1a12'

RUN set -x && \
    # Prep apt so we can install nodejs/npm
    curl -sL https://deb.nodesource.com/setup_5.x | bash - && \
    # Install nodejs/npm
    apt-get install -y nodejs && \
    # Install webpack globally
    npm -g install webpack

RUN set -x && \
    export my_pkg='thekraf' && \
    export my_tag="${thekraf_tag}" && \
    # Clone the git repo
    git clone https://bitbucket.org/carbonizer/${my_pkg} && \
    # Check out specific tag
    ( \
        if [ -n "${my_tag}" ]; then \
            git -C ${my_pkg} checkout ${my_tag}; \
        fi; \
    ) && \
    # Install the pip reqs and gunicorn (the production server)
    # Use --no-cache-dir to keep docker image as slim as possible
    pip3 install --no-cache-dir -r ${my_pkg}/requirements.txt gunicorn && \
    # Install the package in dev mode
    pip3 install --no-cache-dir -e "./${my_pkg}[doc,dev,test]"

RUN set -x && \
    export my_web_path="thekraf/thekraf/flaskapp" && \
    # NOTE: All of the frontend commands are actually redundant because the
    # pre-built bundle is already included in the repo.  The commands are here
    # for reference and to test the workflow.

    # Change to the web folder
    export PUSHED_DIR="$(pwd)" && \
    cd "$my_web_path" && \
    # Install node dependencies
    npm install && \
    # Clean cache to keep docker image slim
    npm cache clean && \
    # Set NODE_PATH so webpack will work
    export NODE_PATH="$(npm root -g)" && \
    # Build the frontend bundle
    webpack && \
    # 'popd-ing' (pushd/popd aparently aren't part of dash, assuming dash is
    # /bin/sh in jessie) is not really necessary since the directory changes
    # won't persist beyond this docker command.  However, it allows these
    # commands to stand alone should additional command be added later.
    cd "$PUSHED_DIR" && \
    # Precache score options
    thekraf reset score_options

RUN set -x && \
    export my_pkg='penguinor-com' && \
    export my_tag="${tag}" && \
    # Clone the git repo
    git clone https://bitbucket.org/carbonizer/${my_pkg} && \
    # Check out specific tag
    ( \
        if [ -n "${my_tag}" ]; then \
            git -C ${my_pkg} checkout ${my_tag}; \
        fi; \
    ) && \
    # Install the pip reqs and gunicorn (the production server)
    # Use --no-cache-dir to keep docker image as slim as possible
    pip3 install --no-cache-dir -r ${my_pkg}/requirements.txt gunicorn && \
    # Install the package in dev mode
    pip3 install --no-cache-dir -e "./${my_pkg}[doc,dev,test]"

RUN set -x && \
    export my_web_path="penguinor-com/penguinor/site/frontend" && \
    # NOTE: All of the frontend commands are actually redundant because the
    # pre-built bundle is already included in the repo.  The commands are here
    # for reference and to test the workflow.

    # Change to the web folder
    export PUSHED_DIR="$(pwd)" && \
    cd "$my_web_path" && \
    # Install node dependencies
    npm install && \
    # Clean cache to keep docker image slim
    npm cache clean && \
    # Set NODE_PATH so webpack will work
    export NODE_PATH="$(npm root -g)" && \
    # Build the frontend bundle
    webpack && \
    # 'popd-ing' (pushd/popd aparently aren't part of dash, assuming dash is
    # /bin/sh in jessie) is not really necessary since the directory changes
    # won't persist beyond this docker command.  However, it allows these
    # commands to stand alone should additional command be added later.
    cd "$PUSHED_DIR"

EXPOSE 5000

CMD ["/bin/bash", "-c", "eval \"$(penguinor web gunicorn)\""]
